let frutas =["platano", "pera", "uva"];

let [fruta1, fruta2] = frutas;
console.log(fruta1, fruta2);

let [,,fruta3] = frutas;
console.log(fruta3);

//Sobrescribiendo valores
let otraFruta ="Manzana";
console.log(otraFruta);
[otraFruta] = frutas; //se sobrescribe el valor
console.log(otraFruta);


//También sirve para remplazar valores
let a=1;
let b=2;
let temp;

temp=a;
a=b;
b=temp;
console.log(a,b);

[a,b] = [b,a];
console.log(a,b);