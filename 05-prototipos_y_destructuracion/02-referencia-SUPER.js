//La herencia en EC5...
let persona = {
    saludar(){
        return "Hola";
    }
};
let amigo = {
    saludar(){
        return Object.getPrototypeOf(this).saludar.call(this)+", saludos!!";
    }
};

Object.setPrototypeOf(amigo, persona);

console.log(amigo.saludar());

//La herencia en EC6..

let amigo2 = {
    saludar(){
        return super.saludar() +",saludos!!!";
    }
};
Object.setPrototypeOf(amigo2, persona);
console.log(amigo2.saludar());