let colores1 = ["rojo",["verde","amarillo"],"morado","naranja",["negro",["blanco"]]];
//queremos el color rojo y verde...
let [c1, [c2]] = colores1;
console.log(c1,c2);
//ahora queremos el amarillo, naranja y blanco
let [,[,c3],,c4,[,[c5]]] = colores1;
console.log(c3,c4,c5);

//También se pueden usa rlos spreads y rest
let colores2= ["rojo","verde","amarillo","morado","naranja","negro","blanco"];
let [colorPrincipal, colorSecundario, ...restoColores] = colores2;
console.log(colorPrincipal);
console.log(colorSecundario);
console.log(restoColores);