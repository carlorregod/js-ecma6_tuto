let gato = {
    sonido(){
        console.log("nyan");
    },
    maullar(){
        console.log("NYAAN!");
    },
};

let perro = {
    sonido(){
        console.log("Wooof!");
    }
};

let angora = Object.create(gato);
console.log(Object.getPrototypeOf(angora) === gato); //true, si es gato

angora.sonido();
angora.maullar();
//Pasamos los prototipos a otro
Object.setPrototypeOf(angora, perro);

console.log(Object.getPrototypeOf(angora) === gato); //true, si es gato

angora.sonido();
angora.maullar();