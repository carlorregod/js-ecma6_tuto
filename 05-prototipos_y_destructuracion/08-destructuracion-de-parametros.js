function crearJugador(nick,opciones){
    opciones = opciones || {};

    let hp  = opciones.hp,
        sp= opciones.sp,
        clase=opciones.clase;

    console.log(nick, hp,sp,clase);
}

crearJugador("Strider",{
    hp:100,
    sp:50,
    clase:"Mago"
});

//En EC6
function crearJugador2(nick,{hp,sp,clase} ){
    console.log(nick, hp,sp,clase);
}
function crearJugador3(nick,{hp,sp,clase}={} ){
    console.log(nick, hp,sp,clase);
}
function crearJugador4(nick,{hp,sp,clase}={hp:100, sp:50, clase:"Mago"} ){
    console.log(nick, hp,sp,clase); //Le damos valores por defecto
}

crearJugador3("Strider"); //Parámetros con muchos undefined
//Strider undefined undefined undefined
crearJugador4("Strider"); //Parámetro correcto
crearJugador4("Striker", {hp:500, sp:100, clase:"Warrior"} ); //Parámetro correcto
crearJugador2("Strider"); //Da error

