let fruta = ["platano"];

let [fruta1, fruta2] = fruta;
console.log(fruta2);// da undefined por default

let [fruta3, fruta4="pera"] = fruta;
console.log(fruta4);  //acá da pera

//Destructuración de objetos
let opciones = {
    nombre: "Carlos"
};

let {nombre, nombre2} = opciones;
console.log(nombre2); //da undefined
let {nombre3, nombre4="Loreto"} = opciones;
console.log(nombre4); //da Loreto, el valor por default
