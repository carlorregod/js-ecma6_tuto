let ajustes = {
     nombre: "Carlos Orrego",
     email: "carlos@orrego.cl",
     facebook: "carlorregod69",
     google: "carlos.orrego.69",
     premium:  true
}

//en EC5
// let nombre = ajustes.nombre,
// email= ajustes.email,
// premium=ajustes.premium,
// google=ajustes.google;
// console.log(nombre, email, premium, google);

//en EC6
 let {nombre, email,premium,google,depago} = ajustes;
console.log(nombre, email, premium, google, depago);
//tambien se puede:
let {nombre:nombre2, email:email2,premium:premium2,google:google2} = ajustes;
console.log(nombre2, email2, premium2, google2);

//también se pueden inicializar vatiables dentro de la destructuración
let {alias="carlos69", nombre:nombre3="Carloco", twitter:cuentaTwi="car82"} = ajustes;
console.log(alias,nombre3, cuentaTwi);
