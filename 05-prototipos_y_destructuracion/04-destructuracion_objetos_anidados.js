//Objetos que tienen objetos

let autoSave = {

    archivo: "app.js",
    cursor: {
        linea:7,
        columna:16
    },
    ultimoArchivo:{
        archivo: "index.html",
        cursor: {
            linea:8,
            columna: 20
        }
    },
    otroNodo:{
        subNodo:{
            cursor:{
                linea:11,
                columna:11,
            }
        }
    }

};

let { cursor: cursorActivo } = autoSave;
console.log(cursorActivo);

let { ultimoArchivo: {cursor: ultimoArchivo} } = autoSave;
console.log(ultimoArchivo);

let { ultimoArchivo: { cursor: {linea:la} } } = autoSave;
console.log(la);

let {otroNodo:{subNodo:{cursor:otroCursor}}} =autoSave;

console.log(otroCursor);
