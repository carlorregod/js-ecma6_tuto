//Definiendo estáticos y métodos conmutados
let nombreMetodo ="conmutadoMetodo";

class Persona{
    constructor(nombre){
        this.nombre =nombre;
    }

    decirNombre(){
        console.log(this.nombre);
    }

    static crear(nombre){
        return new Persona(nombre);
    }

    static hola(){
        return "Hola";
    }
    //Declarando el método conmutado
    [nombreMetodo](){
        console.log(this.nombre.toUpperCase());
    }
}
//Notar los retornos
let yo =Persona.crear("Carlos");
console.log(yo);
console.log(Persona.hola());
yo.conmutadoMetodo();
//yo.crear("john"); //Dará error isn't a function