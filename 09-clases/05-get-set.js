class Persona{

    constructor(nombre, edad){
        this.nombre = nombre;
        this.edad = edad;
    }

    //Seteos personales con set
    set comida(food){
        this.fod = food;
        //la forma más tradicional sería this.food = food
    }

    get frase(){
        return `Mi nombre es ${this.nombre} y tengo ${this.edad} años. Mi comida favorita es ${this.fod || 'ninguna'}`;
    }
}

//Se tiene una clase con constructor y un seteo personalizado.
//Esto sirve mucho para encapsular cosas
let yo = new Persona("Carlos",37);
console.log(yo.frase);
yo.comida = 'Espinaca';
console.log(yo.frase); //¿Notan el cambio?
yo.fod = 'Remolacha'; //También se puede definir de esta manera
//Como forma tradicional, yo.food='Remolacha';
console.log(yo.frase); //Y funciona de la misma forma