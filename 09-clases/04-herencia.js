//Clase padre
class Rectangulo{

    constructor(alto, largo){
        this.alto = alto;
        this.largo=largo;
    }

    getArea(){
        return this.largo*this.alto;
    }

    saludo(){
        return `Hola desde el padre`;
    }

}
//Clase hija
//extends + nombre de clase es agregado para especificar que esta hija heredará lo del padre.
class Cuadrado extends Rectangulo{
    constructor(lado){
        super(lado,lado); //acá super "llamará" al constructor del Rectangulo y seteará los valores de alto y largo, con lado 
    }

    saludo(){
        return `Hola desde el hijo`; //Sobrescribiendo método
    }
}
let rectangulo = new Rectangulo(3,2);
console.log(rectangulo.getArea());
console.log(rectangulo.saludo());
//Probando el cuadrado..
let cuadrado=new Cuadrado(3);
console.log(cuadrado.getArea());
console.log(cuadrado.saludo());


console.log("Validaciones");
console.log(cuadrado instanceof Cuadrado); //da true obvio
console.log(cuadrado instanceof Rectangulo);//por ser hija de esta clase Rectangulo, da true