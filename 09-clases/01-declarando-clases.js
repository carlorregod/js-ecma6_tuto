//Se puede hacer de esta manera por funciones
//función que crea objetos a contar de una clase
function creaClasePersona(people, a, b){
    return new people(a,b);
}

let persona = creaClasePersona(class
    {
        constructor(nombre, edad){
            this.nombre = nombre;
            this.edad=edad;
        }
        
        letNombrar(){
            return `Mi nombre es ${this.nombre} y mi edad es ${this.edad}`;
        }
    },"Hola",23

);
console.log(persona.letNombrar());

//Otra forma
function creaClase2(clase){
    return new clase();
}
class UnaClase{
    holaMundo(){
        return "Hola mundo";
    }
};

let unaclase = creaClase2(UnaClase);
console.log(unaclase.holaMundo());

//La forma más clásica
class Clase3{
    constructor(nombre,edad){
        this.nombre = nombre;
        this.edad = edad;
    }
    holaMundo2(){
        return `Hola soy ${this.nombre} y tengo ${this.edad} años!`;
    }
};

let clase3 = new Clase3("Javier",21);
console.log(clase3.holaMundo2());

