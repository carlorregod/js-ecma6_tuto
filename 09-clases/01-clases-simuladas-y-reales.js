//En EC5
//una "clase"
function Persona (nombre) {
    this.nombre = nombre;

    this.gritarNombre = function(){
        console.log(this.nombre.toUpperCase());
    }
}
//O se puede usar el prototype
Persona.prototype.decirNombre = function(){
    console.log(this.nombre);
}

let carlos = new Persona("nombre");
carlos.gritarNombre();
carlos.decirNombre();

console.log(carlos instanceof Persona);
console.log(carlos instanceof Object);

//EN EC6
class Persona2{
    constructor(nombre){
        this.nombre = nombre;
    }

    decirNombre(){
        console.log(this.nombre);
    }
    gritarNombre(){
        console.log(this.nombre.toUpperCase());
    }
}

let carlos2 = new Persona2("Carlos");
carlos2.decirNombre();
carlos2.gritarNombre();
//Dan function...de forma curiosa:
console.log(typeof Persona2);
console.log(typeof Persona2.prototype.decirNombre);