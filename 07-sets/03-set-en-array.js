let numeros = [1,2,3,4,5,6,7];
let setNumeros = new Set(numeros);

//Convirtiendo el set en un array:
let arrayNumero = [...setNumeros];

console.log(numeros);
console.log(setNumeros);
console.log(arrayNumero);

//Ejercicio: un arreglo que elimina los duplicados
function eliminaDuplicados(arreglomucho){
    let set = new Set(arreglomucho);
    return [...set];
    //también se puede así
    // return [...new Set(arreglomucho)];
}
let arr = [1,2,1,1,4,5,6,2,1,6];
console.log(eliminaDuplicados(arr));

