let items = new Set();
let arreglo = new Set([1,2,2,4,3,3,3,3,3,5,6,3,2]); //También es válido
console.log(items);
console.log(arreglo);
//Agregando al set
items.add(10);
items.add(0);
items.add("10");
items.add(10); //Este lo ignorará
items.add(7);

//NOtar como uno de los 10a gregados será omitido
console.log(items);
console.log(items.size); //Cantidad de items

//Buscando un elemento                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
console.log(items.has(7));//Da true
console.log(items.has("7"));//Da false

//Removiendo valores
items.delete("10");
console.log(items.size, items);
//Elimine todoo el set
items.clear();
console.log(items.size, items);
