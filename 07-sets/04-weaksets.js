let set = new WeakSet();

let persona1 ={
    nombre1: "Juan Carlos"
};
set.add(persona1);
console.log(set);//No imprime JUan carlos, sino que marcia ana

let persona2 ={
    nombre2: "Marcía Ana"
};

set.add(persona2);
console.log(set);
set.delete(persona1);

console.log(set);