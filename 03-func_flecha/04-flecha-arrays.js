//Un arreglo:
var arreglo = [2,6,1,5,10,12,44,8];
//Ordenando arreglos en Js con el método sort, EC5
var ordenado = arreglo.sort(function(a,b){
    return a-b;
});

console.log(ordenado);

//Ahora con EC6 y funciones flecha:
let ordenadoEC6 = arreglo.sort((a,b)=>a-b);
console.log(ordenadoEC6);