//EN EC5 y anteriores... nuevamente tenemos el problema del this. El cual apunta al objeto global (#document) y no
//al objeto manejador. Para ello se debe encerrar la función y explicarle que this es el manejador 
//mediante un paréntesis en la función anónima y pasandola con bind el this y false para que sea exacto
var manejador = {
    id:"123",
    init: function(){
        document.addEventListener("click",(function(event){
            this.clickEnPagina(event.type);
        }). bind(this),false);
    }, //fin del init
    clickEnPagina: function(type){
        console.log("Manejando "+type+" para el id: "+this.id);
    }
}
manejador.init();
//Retorno: Manejando click para el id: 123
//En EC6
var manejadorEC6 = {
    id:"123",
    init: function(){
        document.addEventListener("click",
            event=>this.clickEnPagina(event.type));
    },
    clickEnPagina: function(type){
        console.log("Manejando "+type+" para el id: "+this.id +" con ECMAS6");
    }
}
manejadorEC6.init();
//Retorno: Manejando click para el id: 123 con ECMAS6