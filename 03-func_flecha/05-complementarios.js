let restar = (a,b) => a-b;
//Cómo saber que restar es una función????
console.log(typeof(restar));
console.log(restar instanceof Function);

//Las funciones de flecha son incompatibles con la sentencia new... descomente para saber el error

//let restando = new restar(1,3); //Dará error, de constructor

//Pero así se puede
let restando_ = restar(1,3);

console.log(restando_);

//arguments no funciona en funciones de flecha, probemos en funciones anónimas
//Acá si sirve
var funcioncita = function(a,b){
    console.log(arguments[0]);
}(); //retorna undefined

//Acá dará error, descomente para verlo
// let funcioncita2 = ((a,b)=>console.log(arguments[0]))();

//Y en este otro caso, ocurrirá algo extraño, al meter una función de flecha dentro de una función "Normal"
function ejemplo(x,y){
    ((a,b)=>{
        console.log(arguments[0]);
    })();    
}

ejemplo(10,20);
//Acá la función flecha si "funciona" pues al estar dentro de una función normal, adquiere como arguments los parámetros de entrada 10 y 20. 
//Que no son el a y b!!