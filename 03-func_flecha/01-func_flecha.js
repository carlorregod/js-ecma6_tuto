//Funcione snormales:
// Una sola variable
let unaVar = una => una;
//Sin variables
let sinVar = ()=> console.log("Hola");

// Más de una variable
var miSuma1 = function(n1,n2){
    return n1+n2;
};

let miSuma2 = (n1,n2) => n1+n2;

console.log(miSuma1(3,6)); //retorna 9
console.log(miSuma2(3,6)); //retorna 9

//Más de un bloque de código
let calculadoraFlecha = (a, b)=>{
    let suma = a+b;
    let resta = a-b
    return `La suma de ${a} y ${b} es de ${suma} y la resta es de ${resta}`;
};

console.log(calculadoraFlecha(6,2));

//Los objetos...
var obtenerLibro = function(id){
    return {
        id:id,
        nombre: "Jarry potranco"
    }
};
let obtenerLibro2 = id => ({id:id, nombre:`Jarry potranco`});

console.log(obtenerLibro(2));
console.log(obtenerLibro2(2));
//Retornan {id: 2, nombre: "Jarry potranco"}
