//Funciones anónimas
//EC5
var hola1=function(saludo){
    return "Hola "+saludo;
}("Carlos");
console.log(hola1);
//EC6
let hola2 =((nombre) => `Hola ${nombre}`)("Melissa");
console.log(hola2);