function tareaAsincrona(){

    let promesa = new Promise( (resolve, reject)=>{
        
        setTimeout(function(){
            console.log("Mensaje asincrono");
            //reject(); //Caso en que todo salga mal
            resolve("OK"); //Caso optimista
        },1500);    
    });

    return promesa;

}
//Como se trabajará con promesas, acá hay que definir las salidas

tareaAsincrona().then(
function(data){
    console.log("Todo "+data);
},
function(){
    console.error("Todo mal");
});

console.log("Código sencuencial");