const empleados = [{
    id:1,
    nombre: "Carlos"
},{
    id:2,
    nombre:"Roxana"
},{
    id:3,
    nombre:"Conny"
}];

const salarios = [{
    id:1,
    salario: 1000
},{
    id:2,
    salario:2000
}];

const getEmpleado = (id)=>{
    "use strict";
    
    const promesa = new Promise( (resolve, reject)=>{
        const empleado = empleados.find(empleado=>empleado.id === id);
        if(!empleado)
            reject("El usuario no existe");
        else
            resolve(empleado);
    } );
   
    return promesa;
    
};

const getSalario = (id)=>{
    "use strict";

    const promesa = new Promise((resolve, reject)=>{

        const salario = salarios.find(salario=>salario.id === id);
        const empleado = empleados.find(empleado=>empleado.id === id);
        if(!empleado)
            reject(`No existe el usuario de id ${id}`);
        else if(!salario && empleado.id)
            reject(`No existe salario para el empleado ${empleado.nombre}`);
        else{
            //salario.nombre = empleado.nombre;
            let respuesta = {};
            respuesta = {...empleado, ...salario};
            resolve(null,respuesta);
        };

    });
    return promesa;
};
//Lo mismo pero pasando como parámetro al empleado
const getSalarioPorEmpleado = (empleado)=>{
    'use strict';
    
    return new Promise((resolve, reject)=>{

        const salario2 = salarios.find(salario=>salario.id === empleado.id);
        if(!salario2)
            reject(`No se encuentra el salario del empleado ${empleado.nombre}`);
        else
            resolve({
                nombre:empleado.nombre,
                salario:salario2.salario,
                id:empleado.id
            });
        });

};

    
//Consumiendo las promesas....

getEmpleado(1)
.then(empleado=>{
    getSalarioPorEmpleado(empleado)
    .then(salario=>{
        console.log(salario);
    })
    .catch(err=>{
        console.log(err);
    })
})
.catch(error=>{
    console.log(error);
});

//Otra forma más sencilla

getEmpleado(2)
.then(empleado=>{
    return getSalarioPorEmpleado(empleado);
})
.then(respuesta=>{
        console.log(respuesta);
    })
.catch(error=>{
    console.log(error);
});

//Más formas
getEmpleado(1)
.then(getSalario)
.then(console.log)
.catch(err=>{
    'use strict';
    console.log(err);
});







