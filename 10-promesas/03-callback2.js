const empleados = [{
    id:1,
    nombre: "Carlos"
},{
    id:2,
    nombre:"Roxana"
},{
    id:3,
    nombre:"Conny"
}];

const salarios = [{
    id:1,
    salario: 1000
},{
    id:2,
    salario:2000
}];

const getEmpleado = (id,callback)=>{
    "use strict";
    const empleado = empleados.find(empleado=>empleado.id === id);
    if(!empleado)
        callback("El usuario no existe");
    else
        callback(null, empleado);
    
};

const getSalario = (id,callback)=>{
    "use strict";
    const salario = salarios.find(salario=>salario.id === id);
    const empleado = empleados.find(empleado=>empleado.id === id);
    if(!empleado)
        callback(`No existe el usuario de id ${id}`);
    else if(!salario && empleado.id)
        callback(`No existe salario para el empleado ${empleado.nombre}`);
    else{
        //salario.nombre = empleado.nombre;
        let respuesta = {};
        respuesta = {...empleado, ...salario};
        callback(null,respuesta);
    }
};
//Lo mismo pero pasando como parámetro al empleado
const getSalarioPorEmpleado = (empleado, callback)=>{
    'use strict';
    debugger
    const salario2 = salarios.find(salario=>salario.id === empleado.id);
    if(!salario2)
        callback(`No se encuentra el salario del empleado ${empleado.nombre}`);
    else
        callback(null, {
            nombre:empleado.nombre,
            salario:salario2.salario,
            id:empleado.id
        });
    };



getEmpleado(4, (error, empleado)=>{
    "use strict";

    if(error) 
        return console.error(error);

    console.log('empleado:',empleado);

});
getSalario(2, (error, empleado)=>{
    "use strict";

    if(error) 
        return console.error(error);

    console.log('empleado:',empleado);

});

getEmpleado(3, (error, empleado)=>{
    "use strict";

    if(error) 
        return console.error(error);
 getSalarioPorEmpleado(empleado, (err, salario)=>{
        if(err) 
            return console.error(err);
        console.log( `Salario de trabajador ${salario.nombre} es de: $${salario.salario}`);
    });
  

});



















