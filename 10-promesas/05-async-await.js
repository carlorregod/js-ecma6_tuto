'use strict';

const empleados = [{
    id:1,
    nombre: "Carlos"
},{
    id:2,
    nombre:"Roxana"
},{
    id:3,
    nombre:"Conny"
}];

const salarios = [{
    id:1,
    salario: 1000
},{
    id:2,
    salario:2000
}];

//Ahora sí a emplear el async await
//Los async siempre arrojarán una promesa siempre...
const getEmpleado = async(id)=>{
    
    const empleado = empleados.find(empleado=>empleado.id === id);
    if(!empleado)
        throw new Error(`El usuario de id ${id} no existe`); 
    else
        return empleado;
    
};
//Acá usaremos el await o habrá un error de tipo object
const getSalario = async(empleado)=>{
    
    const salario2 = salarios.find(salario=>salario.id === empleado.id);
    if(!salario2)
        throw new Error(`No se encuentra el salario del empleado ${empleado.nombre}`);
    else
        return ({
            nombre:empleado.nombre,
            salario:salario2.salario,
            id:empleado.id
        });

};


const getInfo = async(id) =>{

    const empleado = await getEmpleado(id);
    const rsalario = await getSalario(empleado);
    return `${empleado.nombre} tiene un salario de ${rsalario.salario}`;

};

//Salario exitoso
getInfo(1)
    .then(console.log);
//Sin salario para Conny
getInfo(3)
    .then(console.log);
//No existe usuario
getInfo(7)
    .then(console.log);

//Primero se resuelve el getInfo(1); luego, el getInfo(7) y al final el getInfo(3) ¿porqué?

//Sin ocupar getInfo() no es necesario usar el await
getEmpleado(2)
.then( empleado=>  getSalario(empleado))
.then( rsalario=>console.log(`${rsalario.nombre} tiene un salario de ${rsalario.salario}`))
.catch(err=>console.log(err));