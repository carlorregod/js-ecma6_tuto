


const getUsuarioById = (id, callback) =>{
    'use strict';
    const usuario = {
        nombre: 'Carlos',
        id
    };
    if(id === 20)
        callback(`El usuario con el id ${id} no existe`, usuario);
    else
        callback(null,usuario);
}

getUsuarioById(20, (err,user)=>{
    "use strict";
    if(err){        
        return console.error(err);
    }
    console.log("Usuario "+user.nombre);

})