
function tareasAsincrona(){

    setTimeout(function(){
        console.log("Mensaje asincrono");
        resolve();
        reject();
    },3000);

}

tareasAsincrona();

console.log("Código sencuencial");

/* Curiosamente se ejecutará primero el "Código secuencial" que la función... */

function resolve(){
    console.log("Todo ok");

}
function reject(){
    console.log("Todo mal");
}