let primerNombre = Symbol();
//let segundoNombre = new Symbol(); //NO se definen así
console.log(typeof primerNombre);
//console.log(segundoNombre);  //entregará error
let apellido = Symbol("nick"); //Esto funciona!

let persona = {

};

//Para usa run símbolo debe referirse como propiedad conmitada
persona[primerNombre] ="Carlos";
persona.primerNombre ="Jojo";
persona[apellido]="Soto"
console.log(persona.primerNombre);
console.log(persona[primerNombre]);
console.log(persona[apellido]);
console.log(persona);
//Esto imprime {primerNombre: "Jojo", Symbol(): "Carlos", Symbol(nick): "Soto"}


let simbolo1 = Symbol("simbolo");
let simbolo2 = Symbol("simbolo");
//Todos son falses pues el symbol son únicos
console.log(simbolo1 == simbolo2);
console.log(simbolo1 === simbolo2);
console.log(Object.is(simbolo1,simbolo2));
console.log(typeof(simbolo1));
console.log(typeof(simbolo2));

let nombrecito = Symbol();
console.log("Nombre es "+nombrecito); //Da error, no puede convertirse un simbolo a string
