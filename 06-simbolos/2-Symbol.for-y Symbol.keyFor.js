let identificador = Symbol.for("identificador");

let objeto = {};
objeto[identificador] = "12345";


console.log(typeof identificador);

let identificador2 = Symbol.for("identificador");
objeto[identificador2] = "abcdef";

console.log(objeto);
//Acá se emplea el identificador2 pues, el 1 ya usa el mismo id del identificador

//Acá el symbol.for si es true
console.log(Object.is(identificador,identificador2));
console.log(identificador == identificador2);
console.log(identificador === identificador2);

//Recupere el identificador del Symbol.for con Symbol.keyFor()
console.log(Symbol.keyFor(identificador));
console.log(Symbol.keyFor(identificador2));

//Pero si se usa...
let simbolo = Symbol("identificador");
console.log(Symbol.keyFor(simbolo)); //Acá da undefined por no ser definido previamente con el .for

