let id = Symbol.for("id");

let numero=10;
let texto="10";
let bool=true;
let notANumber = NaN;

console.log(numero + texto); //1010
console.log(numero+ Number(texto)); //20, el texto se convierte en numero
console.log(numero + notANumber); //NaN
console.log(bool + bool); //Da 2 pues, convierte true en 1 y false en 0
console.log(bool + texto); //Da true10, concatena el true con el 10 en texto

//Para imprimir...
console.log(texto,id);
//Si se desea imprimir, se debe parsear implícitamente
console.log(texto+String(id));//No muy usado

// Acá los errores... no se puede operar un symbol con otro caracter
console.log(texto+id);


