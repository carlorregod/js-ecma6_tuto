
let id = Symbol.for("id");
let activo = Symbol.for("activo");

let persona = {
    [id]:123,
    [activo]:true,
    nombre:"Carlos",
    ["codigo"]:"xyz",
    apellido:"Orregoso",
    edad:37
};

//De esta forma no se reconocen los símbolos al hacer un for..
for (key in persona) {
        console.log(key,persona[key]);
};
//Aunque de forma individual se puede...
console.log(persona[id]);

//Cuando use símbolos hay que usar este bucle

let simbolos = Object.getOwnPropertySymbols(persona);
console.log(simbolos); //Ahhi los imprime... como array  [Symbol(id), Symbol(activo)]

//Ahora sí se puede hacer el for in
for(i in simbolos){
    console.log(simbolos[i], Symbol.keyFor(simbolos[i]),persona[simbolos[i]] );
}