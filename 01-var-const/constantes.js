const PI = 3.141592653589793;
//PI = 3.14;  	// This will give an error
//PI = PI + 10;   // This will also give an error


const cars = ["Saab", "Volvo", "BMW"];
// You can change an element:
cars[0] = "Toyota";
// You can add an element:
cars.push("Audi");

console.log(cars); //Note como audi existe!

//los const tienen las mismas cualidades del let
const PII = 3.1416
 
if (true) {
   const PII = 2.55;
   console.log(PII);
}
console.log(PII);
