//let sólo vive dentro de su entorno, pero var es más "global"
function myFunction() {
    var carName = "Volvo";
    while(true){
        console.log(carName);
        break;
    }
    return console.log(carName);
}

function myFunction2() {
    let carName2 = "Audi";
    while(true){
        console.log(carName2);
        break;
    }
    return console.log(carName2);
}

myFunction();
myFunction2(); 

/*console.log(carName); //Acá hay un error...
console.log(carName2); //Acá arrojará un error. let carName2 vive en el entorno de la función... */