function mezclar( objReceptor, objDonador ){
    Object.keys(objDonador).forEach(function(key){
        objReceptor[key] = objDonador[key];
    });
    return objReceptor;
}

var objReceptor = {};
var objDonador ={
   // nombre: "nombre.ss",
    get nombre2(){
        return "nombre2.ss";
    }
};

console.log(mezclar(objReceptor, objDonador));

console.log(objDonador);

//En EC6:
var objReceptor2 = {};
Object.assign( objReceptor2, objDonador );
console.log(objReceptor2);