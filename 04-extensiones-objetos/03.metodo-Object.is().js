console.log('Caso +0 y -0');

console.log(+0 == -0);         //true
console.log(+0 === -0);        //true
console.log(Object.is(+0,-0)); //false

console.log('Caso NaN y NaN');

console.log(NaN == NaN); //da false
console.log(NaN === NaN);//da false
console.log(Object.is(NaN,NaN)); //true, ambos son no numeros!

console.log('Caso 5  y "5"');

console.log(5=="5");//true
console.log(5==="5");//false
console.log(Object.is(5,"5"));//false