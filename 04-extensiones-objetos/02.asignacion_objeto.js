//Seteando objetos vacíos...EC5
var nombre = {};
var apellido ="apellido";

nombre.nombre = "Carlos";
nombre["segundo nombre"]="Andrés";
nombre[apellido] ="Orrego";

console.log(nombre);
//Retorna: {nombre: "Carlos", segundo nombre: "Andrés", apellido: "Orrego"}

//añadido e ecmascript6...usar nombres de variables de objeto con otras variables
var apellido ="Primer apellido";
var persona = {
    "Primer nombre":"Carlos",
    [apellido] : "Orrego"
};
    
console.log(persona);
//retorna: {Primer nombre: "Carlos", Primer apellido: "Orrego"}


/*  Veamos los subfijos.. de las propiedades conputadas o procesadas */

var sufijo = "Nombre";

var persona2 = {
    ['Primer ' +sufijo]:"Meli",
    ['Segundo '+sufijo]:"ssa"
};

console.log(persona2["Primer Nombre"]);
console.log(persona2["Segundo "+sufijo]);