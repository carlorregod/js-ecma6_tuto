//en ec5...
function crearObjetoEC5(nombre, apellido, edad){
    return {
        nombre:nombre,
        apellido:apellido,
        edad:edad
    }
}
var persona = crearObjetoEC5("Melissa","Ortiz",32);
console.log(persona);
//En EC6 si el nombre de la propiedad es igual al valor de la propiedad, peude omitirs ela doble declaración.
function crearObjetoEC6(nombre, apellido, edad){
    return {
        nombre,
        apellido,
        edad
    }
}
var persona2 = crearObjetoEC6("Melissa","Ortiz",32);
console.log(persona2);

//Y se pueden agregar métodos concisos
let objetoConciso = {
    nombre: "Carlos",
    //forma normal
    muestraNombre: function(){
        return this.nombre;
    },
    //forma concisa
    muestraNomConciso(){
        return this.nombre;
    }
};
console.log(objetoConciso.muestraNombre());
console.log(objetoConciso.muestraNomConciso());

