var saludo = "Hola mundo";
 
//Método startsWith, para saber si el caracter empieza con...
console.log(saludo.substr(0, 1) === 'H'); //EC5, da true
console.log(saludo.startsWith('H')); //EC6, da true
console.log(saludo.startsWith('mu', 5));//da true, se puede entregar un segundo parámetro para setear el inicio del string, por eso de true, es "mundo"

//Método endsWith, para saber si finaliza con...
console.log(saludo.endsWith('do')); //EC6, da true. Se puede señalar el inicio del caracter con un numero de segundo parámetro..

//Método includes, para saber si está el caracter dentro del string
console.log(saludo.includes('x')); //EC6, da false, no hay x
console.log(saludo.includes('la')); //EC6, da true
console.log(saludo.includes('l')); //EC6, da true

//Método repeat, para repetir strings
console.log(saludo.repeat(5)); //Retorna HolaHolaHolaHolaHola
console.log("asd".repeat(3)); //retorna asdasdasd

