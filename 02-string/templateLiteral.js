let nombre = "María",
   apellido = "Pérez";
 
//EC5
console.log("El apellido de " + nombre + " es: " + apellido);
//EC6
console.log(`El apellido de ${nombre} es: ${apellido}`);
//El apellido de María es Pérez


let nombre1 = "María",
   nombre2 = 'María',
   nombre3 = `María`;
 
console.log((nombre1 === nombre2) && (nombre2 === nombre3));


//ocupar \n y no ocuparlos para saltos de línea
console.log(`<h1>Hola</h1>
<p>Mundo</p>`);

//tags
function etiqueta() {
    return console.log(arguments); //ebtregará un array con los argumentos usados con el template literal
 }
//Una buena idea es colocar un debugger dentro del método etiqueta2
//los literales son las palabras en un arreglo, las sustituciones es un arreglo con los ${} del template, desde el 0
 function etiqueta2(literales, ...sustituciones) {
    let oracion = "";
    for (let i = 0; i < sustituciones.length; i += 1) {
        oracion += literales[i];
        oracion += sustituciones[i];
    }
    return oracion;
 }
 
  
 let cantidad = 5;
 let costo = 4000;
  
 let literal = etiqueta `${cantidad} lápices cuestan $${costo}`; //El tag acá es etiqueta que se trabaja como función
 let literal2 = etiqueta2 `${cantidad} lápices cuestan $${costo}`; 
  
 literal; //Retornará un array con los valores
 console.log(literal2); //Retornará la frase construida con los elementos del array de arguments


