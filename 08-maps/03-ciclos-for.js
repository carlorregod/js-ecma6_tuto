let mapa = new Map([ ["nombre","Carlos"],["edad",37] ]);

//CICLO FOREACH

mapa.forEach(function(valor, llave, mapaOrigen){
    console.log(`Llave es ${llave} y valor es ${valor}`);
    console.log(mapaOrigen);
});
mapa.forEach((valor, llave, mapaOrigen)=>{
    console.log(`Llave es ${llave} y valor es ${valor}`);
    console.log(mapaOrigen);
});
console.log("Ciclos for-on");
//CIiclo FOR-OF
let numeros = [19, 100, 32, 13];
//recorriendo ... for normal
for(let i=0;i<numeros.length;i++){
    console.log(numeros[i]);
}
//recorriendo.. con for in
for (let i in numeros) {
    console.log(numeros[i]);
}
//recorriendo... forEach
numeros.forEach((valor, llave, set)=>{
    console.log(valor);
})
//recorriendo con el for-of
for (let numero of numeros) {
    console.log(numero);
}
//Ejemplos correctos.
let personas = [
    {nombre: "Carlos", edad:30},
    {nombre: "María", edad:23},
    {nombre: "Victor", edad:11},
    {nombre: "Tommy", edad:19},
];

for (const per of personas) {
    console.log(per.nombre, per.edad);
}

let personas1 = new Set();
personas1.add( {nombre: "Carlos", edad:30});
personas1.add( {nombre: "Tommy", edad:39});
personas1.add( {nombre: "Nyan", edad:5});

for (const p of personas1) {
    console.log(p.nombre, p.edad);
}

let personas2 = new Map([ ["nombre","Carlos"],["edad","Nyan"] ]);

for (p of personas2) {
    console.log(p);
}
