let mapaInicializado = new Map([ ["hola","mundo"],["nyan","gato"],[null,undefined] ]);
console.log(mapaInicializado);
console.log(mapaInicializado.get(null)); //efectivamete cualquier valor del mapa puede ser "agarrado, incluso undefined!"

//valores raros
mapaInicializado.set(undefined,NaN);
console.log(mapaInicializado);
