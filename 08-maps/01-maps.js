let mapa = new Map();
// let mapaInicializado = new Map(["hola2"]"mundo2",["hola3"]"mundo3");
mapa.set("hola","mundo");
mapa.set("nyan","gato");
console.log(mapa);
// console.log(mapaInicializado);
console.log(mapa.has("nyan")); //Se consulta por el valor de la librería, da true
console.log(mapa.has("gato")); //Se consulta por el valor de la librería, da FALSE!
console.log(mapa.get("nyan"));//Se obtiene gato porque es el valor que tiene asignado el nyan
console.log(mapa.get("gato"));//Se obtiene undefined porque no existe la llave de gato
console.log(mapa.size); //Tamaño de la librería, da 2

//El foreach
console.log("Recorriendo con un foreach");
mapa.forEach((valor, llave)=>{
    console.log(llave, valor);
});
//
console.log("Testeando los borrados");
mapa.delete("gato"); 
console.log(mapa); //Se debe borrar por nombre de librería, gato no es librería
mapa.delete("nyan"); 
console.log(mapa); //ahora sí borra al gato...
mapa.clear(); //Borra toda la colección
console.log(mapa); 

