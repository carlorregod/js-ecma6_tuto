# Curso JS.ECMA6

Estos son los apuntes para generar codigo fuente asociado para el aprendizaje de lo nuevo que tiene JS con ECMA6 se recomienda instalar:

* NodeJs 8 o superior

## Instalación

Sólo ejecutar git clone con este link

```
git clone https://gitlab.com/carlorregod/js-ecma6_tuto.git
```

Se recomienda utilizar Visual Studio Code junto al plugin Live Action.

## Tecnologías

* Javascript EC6


### ( ͡° ͜ʖ ͡°) ###

* ( ͡° ͜ʖ ͡°)
* ( ͡° ͜ʖ ͡°)
* ( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)
* ( ͡° ͜ʖ ͡°)
